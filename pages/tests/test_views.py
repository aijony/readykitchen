# pages/tests/test_views.py

from django.test import TestCase
from django.urls import reverse


class TestView(TestCase):

    def test_home_url_by_path(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_url_by_name(self):
        response = self.client.get(reverse('pages:home'))
        self.assertEqual(response.status_code, 200)

    def test_home_template(self):
        response = self.client.get(reverse('pages:home'))
        self.assertTemplateUsed(response, 'pages/home.html')

    def test_home_contains(self):
        response = self.client.get(reverse('pages:home'))
        self.assertContains(response, 'ReadyKitcheN')
