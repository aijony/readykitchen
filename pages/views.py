# pages/views.py


from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.urls import reverse_lazy

from .forms import ContactForm


class HomeView(TemplateView):
    template_name = 'pages/home.html'


class AboutView(TemplateView):
    template_name = 'pages/about.html'


class ContactView(FormView):
    template_name = 'pages/contact.html'
    form_class = ContactForm
    # success_url = "/"
    success_url = reverse_lazy('pages:home')

    def form_valid(self, form):
        form.send_mail()
        return super().form_valid(form)
