# comments/models.py

from django.db import models
from django.contrib.auth import get_user_model
from recipes.models import Recipe


class Comment(models.Model):
    recipe = models.ForeignKey(
        Recipe,
        on_delete=models.CASCADE,
        related_name='comments',
    )
    message = models.CharField(max_length=255)
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.message

    def get_absolute_url(self):
        return reverse('recipe:recipe_detail')
