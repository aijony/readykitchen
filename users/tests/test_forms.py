# users/tests/test_forms.py

from django.test import TestCase
from django.contrib.auth import get_user_model


class TestForm(TestCase):

    username = 'newuser'
    email = 'newuser@email.com'

    def test_signup_form(self):
        new_user = get_user_model().objects.create_user(
            self.username, self.email
        )
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(get_user_model().objects.all()
                         [0].username, self.username)
        self.assertEqual(get_user_model().objects.all()[0].email, self.email)
