# users/tests/test_views.py

from django.test import TestCase
from django.urls import reverse


class TestView(TestCase):

    def test_signup_url_by_path(self):
        response = self.client.get('/users/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_by_name(self):
        response = self.client.get(reverse('users:signup'))
        self.assertEqual(response.status_code, 200)

    def test_signup_template(self):
        response = self.client.get(reverse('users:signup'))
        self.assertTemplateUsed(response, 'users/signup.html')

    def test_home_contains(self):
        response = self.client.get(reverse('users:signup'))
        self.assertContains(response, 'ReadyKitcheN')
