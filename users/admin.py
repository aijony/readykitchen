# users/admin.py

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChageForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChageForm
    model = CustomUser
    # Display only the following fields for the user
    # list_display = ['username', 'email', 'is_staff']


# Register your models here
admin.site.register(CustomUser, CustomUserAdmin)
