# users/forms.py

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        # default fields for creating new user
        # fields = UserCreationForm.Meta.fields
        # explicitely specify fields for creating new user
        fields = ('username', 'email')


class CustomUserChageForm(UserChangeForm):

    class Meta:
        model = CustomUser
        # fields = UserChangeForm.Meta.fields
        # explicitely specify fields for editing user
        fields = ('username', 'email')
