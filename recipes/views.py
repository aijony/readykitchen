# recipes/views.py

from .forms import EmailPostForm
from .models import Recipe
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.shortcuts import render, get_object_or_404


class RecipeListView(LoginRequiredMixin, ListView):
    model = Recipe
    paginate_by = 2
    template_name = 'recipes/recipe_list.html'
    login_url = 'login'  # restrict access to non-registered user


class RecipeDetailView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = 'recipes/recipe_detail.html'
    login_url = 'login'


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    fields = ('title', 'subtitle', 'ingredients', 'procedure', 'tags')
    template_name = 'recipes/recipe_new.html'
    login_url = 'login'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    fields = ('title', 'subtitle', 'ingredients', 'procedure', 'tags')
    template_name = 'recipes/recipe_edit.html'
    login_url = 'login'

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = 'recipes/recipe_delete.html'
    success_url = reverse_lazy('recipes:recipe_list')
    login_url = 'login'

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


# function-based view for sharing recipe via email
def recipe_share(request, recipe_id):

    # Retrieve recipe by id
    recipe = get_object_or_404(Recipe, id=recipe_id)
    sent = False

    if request.method == 'POST':
        # Form was submitted
        form = EmailPostForm(request.POST)

        if form.is_valid():
            # Form fields passed validation
            cd = form.cleaned_data
            recipe_url = request.build_absolute_uri(recipe.get_absolute_url())
            subject = '{}({}) recommends you reading "{}"'.format(
                cd['name'],
                cd['email_from'],
                recipe.title
            )
            message = 'Read "{}" at {}\n\n{}\'s comments: {}'.format(
                recipe.title,
                recipe_url,
                cd['name'],
                cd['message']
            )
            send_mail(subject, message,
                      'admin@readykitchen.com', [cd['email_to']])
            sent = True
    else:
        form = EmailPostForm()

    return render(request, 'recipes/recipe_share.html', {'recipe': recipe,
                                                         'form': form,
                                                         'sent': sent})
