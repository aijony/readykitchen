# recipes/models.py

from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy


class RecipeTag(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class RecipeManager(models.Manager):
    pass


class Recipe(models.Model):
    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=355)
    ingredients = models.CharField(max_length=355)
    procedure = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    tags = models.ManyToManyField(RecipeTag, blank=True)
    # ingredients = models.ManyToManyField(RecipeIngredient, blank=True)

    objects = RecipeManager()  # Manager

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('recipes:recipe_detail', args=[str(self.id)])


class RecipeImage(models.Model):
    recipe = models.ForeignKey(
        Recipe, on_delete=models.CASCADE,
        related_name='recipeimages',)
    image = models.ImageField(upload_to="recipe-images")
    thumbnail = models.ImageField(upload_to="recipe-thumbnails", null=True)
