# recipes/forms.py

from django import forms


# for sharing recipe to someone via email
class EmailPostForm(forms.Form):

    name = forms.CharField(max_length=50)
    email_from = forms.EmailField()
    email_to = forms.EmailField()
    message = forms.CharField(required=False, widget=forms.Textarea)
