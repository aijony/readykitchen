# recipes/admin.py


from django.contrib import admin

from django.utils.html import format_html

from .models import Recipe, RecipeTag, RecipeImage


class RecipeAdmin(admin.ModelAdmin):

    list_display = ('title', 'subtitle', 'ingredients', 'author')
    list_filter = ('title', 'author', 'date')
    list_editable = ('subtitle', 'ingredients',)
    search_fields = ('title',)


admin.site.register(Recipe, RecipeAdmin)


class RecipeTagAdmin(admin.ModelAdmin):

    list_display = ('name', 'id')
    search_fields = ('name',)


admin.site.register(RecipeTag, RecipeTagAdmin)


class RecipeImageAdmin(admin.ModelAdmin):

    list_display = ('thumbnail_tag', 'recipe_title', 'image')
    readonly_fields = ('thumbnail',)
    search_fields = ('recipe__title',)

    def thumbnail_tag(self, obj):
        if obj.thumbnail:
            return format_html('<img src="%s"/>' % obj.thumbnail.url)
        return "-"
        thumbnail_tag.short_description = "Thumbnail"

    def recipe_title(self, obj):
        return obj.recipe.title


admin.site.register(RecipeImage, RecipeImageAdmin)
