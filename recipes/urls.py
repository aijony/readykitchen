# recipes/urls.py


from django.urls import path

from .views import (
    RecipeListView,
    RecipeDetailView,
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDeleteView,
    recipe_share,
)

app_name = 'recipes'
urlpatterns = [
    path('', RecipeListView.as_view(), name='recipe_list'),
    path('<int:pk>/', RecipeDetailView.as_view(), name='recipe_detail'),
    path('new/', RecipeCreateView.as_view(), name='recipe_new'),
    path('<int:pk>/edit/', RecipeUpdateView.as_view(), name='recipe_edit'),
    path('<int:pk>/delete/', RecipeDeleteView.as_view(), name='recipe_delete'),
    path('<int:recipe_id>/share/', recipe_share,
         name='recipe_share'),  # fuction-based view is called
]
